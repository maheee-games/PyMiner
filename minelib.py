import random

class MineField( object ):

    class Field:
        def __init__(self):
            self.open = False
            self.mine = False
            self.flagged = False
            self.info = 0
        
        def __str__(self):
            return "Open: "+str(self.open)+", Mine: "+str(self.mine)+", Flagged: "+str(self.flagged)+", Info: "+str(self.info)

            
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.setMineRatio(10)
        
        self.field = []
        self.field.extend([self.__getEmptyColumn() for i in range(width)])
        
        self.removedColumn = self.__getEmptyColumn()
    
    
    def __updateSampleSet(self):
        self.sampleSet = list()
        self.sampleSet.extend(0 for i in range(10*self.ratio))
        self.sampleSet.extend(1 for i in range(10))
        random.shuffle(self.sampleSet)
    
    def __getEmptyColumn(self):
        emptyColumn = list()
        emptyColumn.extend([self.Field() for i in range(self.height)])
        return emptyColumn
    
    def __createColumn(self):
        c = self.__getEmptyColumn()
        s = random.sample(self.sampleSet, self.height)
        for i in range(self.height):
            if s[i] == 1:
                c[i].mine = True
        return c
    
    def __calculateInfos(self, colNr):
        for y in range(self.height):
            if not self.get(colNr, y).mine:
                self.get(colNr, y).info = self.__calculateInfo(colNr, y)
            
    def __calculateInfo(self, x, y):
        mines = 0
        for c in self.__getSurroundingCoordinates(x,y):
            if self.get(*c).mine: mines += 1
        
        return mines
        
    def __countFlagsAndOpenMines(self, x, y):
        flags = 0
        for c in self.__getSurroundingCoordinates(x,y):
            if self.__isFlagOrOpenMine(*c): flags += 1
        
        return flags
        
    def __isFlagOrOpenMine(self, x, y):
        f = self.get(x,y)
        if f.flagged: return True
        if f.mine and f.open: return True
        return False
    
    def __rekOpen(self, x, y):
        if not self.isInBounds(x,y):
            return
        if self.field[x][y].open or self.field[x][y].mine:
            return
        
        self.field[x][y].open = True
        
        if self.field[x][y].info == 0:
            for c in self.__getSurroundingCoordinates(x,y):
                self.__rekOpen(*c)
    
    def __getSurroundingCoordinates(self, x, y):
        return ((x+1, y+1),(x+1, y-1),(x+1, y),(x-1, y+1),(x-1, y-1),(x-1, y),(x, y+1),(x, y-1))
    
    
    def isInBounds(self, x, y):
        if x < 0 or y < 0 or x > self.width-1 or y > self.height-1:
            return False
        else:
            return True
    
    def setMineRatio(self, mineRatio):
        self.ratio = mineRatio
        self.__updateSampleSet()
    
    
    def get(self, x, y):
        if self.isInBounds(x,y):
            return self.field[x][y]
        else:
            return self.Field()
    
    def getRemoved(self, y):
        if y >= 0 and y < self.height:
            return self.removedColumn[y]
        else:
            return self.Field()
    
    def addNewColumn(self):
        self.removedColumn = self.field.pop(0)
        self.field.append(self.__createColumn())
                
        self.__calculateInfos(self.width-2)
        
        x = self.width - 3
        
        for y in range(self.height):
            if self.get(x,y).open and not self.get(x,y).mine and self.get(x,y).info == 0:
                self.open(x+1, y+1)
                self.open(x+1, y-1)
                self.open(x+1, y  )
            

    def open(self, x, y):
        """opens field and returns the number of exploded mines
           return -1 if opening didn't happen
        """
        
        if not self.isInBounds(x,y):
            return -1
        
        if self.get(x,y).open or self.get(x,y).flagged:
            return -1
        
        self.__rekOpen(x,y)
        
        if self.get(x,y).mine:
            self.get(x,y).open = True
            return 1
        
        return 0
        
    
    def openNumber(self, x, y):
        """opens fields and returns the number of exploded mines
           return -1 if opening didn't happen
        """
        if not self.isInBounds(x,y) or not self.get(x,y).open or self.get(x,y).mine or self.get(x,y).info == 0:
            return -1
        
        if self.get(x,y).info != self.__countFlagsAndOpenMines(x,y):
            return -1
            
        mines = 0
        
        for c in self.__getSurroundingCoordinates(x,y):
            e = self.open(*c)
            if e > 0:
                mines += e
        

        if mines > 0:
            for c in self.__getSurroundingCoordinates(x,y):
                f = self.get(*c)
                if f.flagged and not f.mine:
                    f.flagged = False
        
        return mines
        
    
    def flag(self, x, y):
        if not self.isInBounds(x,y):
            return -1
        
        if self.get(x,y).open:
            return -1
        
        self.get(x,y).flagged = not self.get(x,y).flagged
        return 0
