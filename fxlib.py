from __future__ import division
import pyglet
from pyglet.gl import *
from pyglet.image import *

class FXLib( object ):
    
    def __init__(self):
        self.img = {}
        self.snd = {}
        self.fnt = {}

    def init(self):
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    def _store_image(self, name, image):
        image.anchor_x = image.width / 2
        image.anchor_y = image.height / 2
        self.img[name] = image

    def loadImageGrid(self, names, fileName, columns, rows):
        image = pyglet.resource.image(fileName)
        imageGrid = ImageGrid(image, rows, columns)
        imageGrid = TextureGrid(imageGrid)
        for y in range(rows):
            for x in range(columns):
                name = names[rows - y - 1][x]
                image = imageGrid[(y * rows) + x]
                self._store_image(name, image)

    def loadImage(self, name, fileName):
        image = pyglet.resource.image(fileName)
        self._store_image(name, image.texture)

    def loadSound(self, name, fileName):
        self.snd[name] = pyglet.resource.media(fileName, streaming=False)

    def loadFont(self, name, font):
        self.fnt[name] = font

    def play(self, snd):
        self.snd[snd].play()
    
    def draw(self, img, x, y):
        image = self.img[img]
        image.blit(x, y)
        
    def __drawText(self, font, size, text, x, y, color1, right=False):
        label = pyglet.text.Label(
            text,
            font_name=font,
            font_size=size,
            x=x, y=y,
            color = color1,
            anchor_x= ((right and 'right') or 'left'),
            anchor_y='top'
        )

        label.draw()
        
    def drawText(self, fnt, text, x, y, right=False):
        fnt = self.fnt[fnt]
        self.__drawText(fnt['font'], fnt['size'], text, x, y, fnt['color'], right)
