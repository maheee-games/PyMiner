import os, sys, pyglet, minelibext, statelib


class MenuGameState( statelib.GameState ):
    def onMouseMotion(self, x, y, dx, dy):
        pass
        
    def onMouseRelease(self, x, y, button, modifiers):
        pass
        
    def onDraw(self):
        pass
        
    def onUpdate(self, dt):
        self.exitMethod('game')


class EndlessDeminingGameState( statelib.GameState ):
    def __init__(self, opt, gameOpt, setting):
        
        self.opt = opt
        self.gameOpt = gameOpt
        self.setting = setting
        
    def reset(self):
        self.lives = 5
        self.settingPos = 0
        
        self.fxlib = self.globalState['fxlib']
        
        self.mf = minelibext.MineFieldWrapper(self.gameOpt, self.fxlib)
        self.mf.manuallyAddNewColumns(10)
        self.mf.manuallyOpenLeftSide()
        
    def onKeyPress(self, symbol, modifiers):
        if self.lives > 0:
            if symbol == pyglet.window.key.SPACE:
                self.mf.setSpeed( self.opt['fastSpeed'] )
                    
    def onKeyRelease(self, symbol, modifiers):
        if self.lives > 0:
            if symbol == pyglet.window.key.SPACE:
                self.mf.setSpeed( self.setting[self.settingPos][1] )
        
            if symbol == pyglet.window.key.ESCAPE:
                self.exitMethod('menu')
        
        if self.lives <= 0:
            self.exitMethod('menu')
            
            
    def onMouseMotion(self, x, y, dx, dy):
        self.mf.setMousePos(x, y)
    
    def __playActionSound(self, action):
        if action < 0: self.fxlib.play('hit')
        elif action == 0: self.fxlib.play('ping')
        else: self.fxlib.play('explosion')
            
    def onMouseRelease(self, x, y, button, modifiers):
        self.mf.setMousePos(x, y)
        if self.lives > 0:
            if button == pyglet.window.mouse.LEFT:
                self.__playActionSound(self.mf.leftClick())
                
            elif button == pyglet.window.mouse.MIDDLE:
                self.__playActionSound(self.mf.middleClick())
                
            elif button == pyglet.window.mouse.RIGHT:
                self.__playActionSound(self.mf.rightClick())
                
    def onDraw(self):
        #Minenfeld
        self.mf.drawField()
        
        #Anzahl an Leben
        for x in range(5):
            imgx = 640 + (x * 30)
            imgy = 530
            if self.lives > x:
                self.fxlib.draw('flagOk', 640 + (x * 30) + 15, 530 - 15)
            else:
                self.fxlib.draw('flag', 640 + (x * 30) + 15, 530 - 15)        

        #Maske
        self.fxlib.draw('mask', 400, 300)

        #Texte
        self.fxlib.drawText('normal', str(self.mf.getCleared()), 755, self.opt['height']-31, True)
        if self.settingPos < 10:
            self.fxlib.drawText('small', "0"+str(self.settingPos), 780, self.opt['height']-31, True)
        else:
            self.fxlib.drawText('small', str(self.settingPos), 780, self.opt['height']-31, True)
            
    def onUpdate(self, dt):
        if self.mf.update(dt) > 0:
            self.fxlib.play('explosion')
        
        #Stufe wechseln
        if self.setting[self.settingPos][0] <= self.mf.getCleared():
            self.mf.setSpeed( self.setting[self.settingPos][1] )
            self.mf.setMineRatio( self.setting[self.settingPos][2] )
            self.mf.setPauseAfterError( self.setting[self.settingPos][3] )
            self.settingPos += 1
            self.fxlib.play('powerup')
        
        #Lives
        self.lives = 5 - self.mf.getErrors()
            
        if self.lives == 0:
            self.mf.setSpeed(0)

            
