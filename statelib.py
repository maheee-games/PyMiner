
class GameState( object ):
    def reset(self,):
        pass
    def setGlobalState(self, globalState):
        self.globalState = globalState
    def setExitMethod(self, method):
        self.exitMethod = method
    def onKeyPress(self, symbol, modifiers):
        pass
    def onKeyRelease(self, symbol, modifiers):
        pass
    def onMouseMotion(self, x, y, dx, dy):
        pass
    def onMousePress(self, x, y, button, modifiers):
        pass
    def onMouseRelease(self, x, y, button, modifiers):
        pass
    def onDraw(self):
        pass
    def onUpdate(self, dt):
        pass


class ExitState( GameState ):
    def reset(self,):
        exit()    


class StateManager( object ):
    
    def __init__(self, window, states, startState, globalState = {}):
        self.window = window
        self.states = states
        self.pause = False
        
        self.globalState = globalState
        self.setState(states[startState])
        
    
    def exit(self, code, globalStateUpdate = {}):
        print(code)
        
        self.globalState.update(globalStateUpdate)
        self.setState(self.states[code])
        
        

    def setState(self, state):
        self.state = state
        state.setExitMethod(self.exit)
        state.setGlobalState(self.globalState)
        state.reset()
        
        self.window.on_key_press = state.onKeyPress
        self.window.on_key_release = state.onKeyRelease
        self.window.on_mouse_motion = state.onMouseMotion
        self.window.on_mouse_press = state.onMousePress
        self.window.on_mouse_release = state.onMouseRelease
        self.window.on_draw = state.onDraw
            
        def deactivate():
            self.pause = True
            
        def activate():
            self.pause = False
        
        def close():
            exit()
        
        self.window.on_deactivate = deactivate
        self.window.on_activate = activate
        self.window.on_close = close
        

    def on_update(self, dt):
        if not self.pause:
            self.state.onUpdate(dt)
