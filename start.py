#!/usr/bin/python

import os, sys, pyglet, statelib, gamestates, fxlib

opt = {
    'title': "Miner",
    'width':    800,
    'height':   600,
    'framerate': 30,
    'fastSpeed': 50,
    'font':  "Courier New",
}

gameOpt = {
    'fieldSizeX': 32,
    'fieldSizeY': 16,
    'mineSize':   30,
    'fieldPosX':  50,
    'fieldPosY': 110,
    'rFieldPosX': 10,
    'rFieldPosY':110,
}

setting = (
    # cleared Mines, speed, ratio, pause after error
    (    0,   5, 15,  2),
    (    5,   5, 10,  2),
    (   10,   7, 10,  2),
    (   20,   7,  9,  2),
    (   50,   9,  9,  2),
    (  100,   9,  8,  2),
    (  200,  11,  8,  2),
    (  300,  11,  7,  2),
    (  400,  13,  7,  2),
    (  500,  13,  6,  2),
    (  600,  15,  6,  2),
    (  700,  15,  5,  2),
    (  800,  17,  5,  2),
    (  900,  17,  4,  2),
    ( 1000,  19,  4,  2),
    ( 2000,  19,  3,  2),
    ( 3000,  19,  2,  2),
    ( 4000,  19,  1,  2),
    (9999999,19,  1,  2),
)

states = {
    'exit': statelib.ExitState(),
    'menu': gamestates.MenuGameState(),
    'game': gamestates.EndlessDeminingGameState(opt, gameOpt, setting),
}

def loadEffects():
    fx = fxlib.FXLib()

    fx.loadImageGrid([#21 22 23 24 25
            ['hidden', 'info0',     'info1',       'info2', 'info3'],
            ['info4',  'info5',     'info6',       'info7', 'info8'],
            ['flagOk', 'flag',      'mineSave',    'mine',  '_'    ],
            ['hover',  'hoverDone', 'hoverFlagged', '_',    '_'    ],
            ['_',      '_',         '_',            '_',    '_'    ]
        ], "img/icons.png", 5, 5
    )
    fx.loadImage('mask',         "img/mask.png")

    fx.loadSound('hit',      "snd/hit.wav")
    fx.loadSound('blip',     "snd/blip.wav")
    fx.loadSound('ping',     "snd/ping.wav")
    fx.loadSound('explosion',"snd/explosion.wav")
    fx.loadSound('powerup',  "snd/powerup.wav")

    fx.loadFont('small',  {'font': "Courier New", 'size': 16, 'color': (0,0,0,255)})
    fx.loadFont('normal', {'font': "Courier New", 'size': 24, 'color': (0,0,0,255)})

    return fx

def createWindow(width, height):
    platform = pyglet.window.get_platform()
    display = platform.get_default_display()
    screen = display.get_default_screen()
    try:
        template = pyglet.gl.Config(sample_buffers=1, samples=4)
        config = screen.get_best_config(template)
    except pyglet.window.NoSuchConfigException:
        template = pyglet.gl.Config()
        config = screen.get_best_config(template)
    context = config.create_context(None)
    window = pyglet.window.Window(width, height, context=context)
    return window

def main():
    # Fenster erstellen
    #window = createWindow(opt['width'], opt['height'])
    window = pyglet.window.Window(800, 600)

    fxlib = loadEffects()
    fxlib.init()

    # State-Manager erstellen
    sm = statelib.StateManager(window, states, 'menu', {'fxlib': fxlib})

    # Timer setzen und Anwendung starten
    pyglet.clock.schedule_interval(sm.on_update, 0.01)
    pyglet.app.run()


main()
