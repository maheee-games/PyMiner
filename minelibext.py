from __future__ import division
import minelib

class MineFieldWrapper( object ):

    def __init__(self, options, fxlib):
        self.opt = options
        self.fxlib = fxlib
        
        self.speed = 0
        self.errorPause = 0
        self.addX = 0
        
        self.mouseX = -1
        self.mouseY = -1
        self.mouseFieldX = -1
        self.mouseFieldY = -1
                
        self.progress = 0
        self.errors = 0
        self.cleared = 0

        self.pause = 0
        
        self.field = minelib.MineField(self.opt['fieldSizeX'], self.opt['fieldSizeY'])
        
    def __getField(self):
        x = (self.mouseX + self.addX - self.opt['fieldPosX']) / self.opt['mineSize']
        y = (self.mouseY - self.opt['fieldPosY']) / self.opt['mineSize']
        return int(x), int(y)
        
        
    def __pickFieldImage(self, f):
        if f.flagged:
            return 'flag'
        elif not f.open:
            return 'hidden'
        elif f.mine:
            return 'mine'
        else:
            return 'info' + str(f.info)


    def __pickRemovedFieldImage(self, f):
        if f.flagged and f.mine:
            return 'flagOk'
        elif f.flagged:
            return 'flag'
        elif f.mine and not f.open:
            return 'mine'
        elif f.mine and f.open:
            return 'mineSave'
        else:
            return 'info0'
        
    def __addNewColumn(self):
        self.field.addNewColumn()
        e = 0
        
        for y in range(self.opt['fieldSizeY']):
            f = self.field.getRemoved(y)
            if f.flagged and f.mine:
                self.cleared += 1
            elif f.flagged:
                self.errors += 1
                e += 1
            elif f.mine and not f.flagged and not f.open:
                self.errors += 1
                e += 1
        return e
    
    def manuallyAddNewColumns(self, amount):
        for i in range(amount):
            self.__addNewColumn()
    
    def manuallyOpenLeftSide(self):
        self.field.open(0,0)
    
    def setSpeed(self, speed):
        self.speed = speed
    
    def setMineRatio(self, rat):
        self.field.setMineRatio(rat)

    def setPauseAfterError(self, errorPause):
        self.errorPause = errorPause
    
    def setMousePos(self, x, y):
        self.mouseX = x
        self.mouseY = 600-y
        self.mouseFieldX, self.mouseFieldY = self.__getField()
    
    def getProgress(self):
        return self.progress
    
    def getCleared(self):
        return self.cleared
    
    def getErrors(self):
        return self.errors

    def handleError(self, e):
        if e > 0:
            self.errors += e
            self.pause = self.errorPause * self.speed
        return e
        
    def leftClick(self):
        e = self.field.open(self.mouseFieldX, self.mouseFieldY)
        return self.__handleError(e)
    
    def rightClick(self):
        return self.field.flag(self.mouseFieldX, self.mouseFieldY)
    
    def middleClick(self):
        e = self.field.openNumber(self.mouseFieldX, self.mouseFieldY)
        return self.__handleError(e)
    
    def update(self, tDif):
        e = 0
        
        if tDif > 0:
            movement = tDif * self.speed
            if self.pause > 0:
                self.pause -= movement
                if self.pause < 0:
                    self.addX -= self.pause
                    self.pause = 0
            else:
                self.addX += movement
            
            while self.addX > self.opt['mineSize']:
                self.addX -= self.opt['mineSize']
                e += self.__addNewColumn()
        return e
		
    def drawField(self):
    
        #Feld darstellen
        for x in range(26):
            for y in range(self.opt['fieldSizeY']):
                f = self.field.get(x, y)
                imgx = x * self.opt['mineSize'] - self.addX + self.opt['fieldPosX'] + (self.opt['mineSize']/2)
                imgy = 600 - (y * self.opt['mineSize'] + self.opt['fieldPosY'] + (self.opt['mineSize']/2))
                self.fxlib.draw(self.__pickFieldImage(f), imgx, imgy)
		
                if self.mouseFieldX == x and self.mouseFieldY == y:
                    if f.open and f.info > 0:
                       self.fxlib.draw('hoverDone', imgx, imgy)
                    elif not f.open and f.flagged:
                        self.fxlib.draw('hoverFlagged', imgx, imgy)
                    elif not f.open:
                        self.fxlib.draw('hover', imgx, imgy)
        
        #entfernte Spalte darstellen
        for y in range(self.opt['fieldSizeY']):
            imgx = self.opt['rFieldPosX'] + (self.opt['mineSize']/2)
            imgy = 600 - (y * self.opt['mineSize'] + self.opt['rFieldPosY'] + (self.opt['mineSize']/2))
            self.fxlib.draw(self.__pickRemovedFieldImage(self.field.getRemoved(y)), imgx, imgy)
    
